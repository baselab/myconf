"""""""""""""""""""""""""""""""""""""""""
"       NeoVIM configuration file       "
"""""""""""""""""""""""""""""""""""""""""

function! BuildComposer(info)
  if a:info.status != 'unchanged' || a:info.force
    if has('nvim')
      !cargo build --release
    else
      !cargo build --release --no-default-features --features json-rpc
    endif
  endif
endfunction

"---- Plugin manager
call plug#begin('~/.local/share/nvim/plugged')
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'bronson/vim-trailing-whitespace'
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'junegunn/vim-easy-align'
Plug 'neomake/neomake'
Plug 'blueyed/vim-diminactive'
Plug 'euclio/vim-markdown-composer', { 'do': function('BuildComposer') }
call plug#end()

"---- Options
set number		" Show line number
set numberwidth=5
set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set smartcase		" Do smart case matching
set incsearch		" Incremental search
set autowrite		" Automatically save before commands like :next and :make
set hidden		" Hide buffers when they are abandoned
set mouse=a		" Enable mouse usage (all modes)
set ruler
set hlsearch
set backspace=indent,eol,start
"set backspace=2
set noswapfile
set history=50

"-- Splits
set splitbelow
set splitright
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
" Max out the height of the current split
"ctrl + w _
" Max out the width of the current split
"ctrl + w |
" Normalize all split sizes, which is very handy when resizing terminal
"ctrl + w =
" Swap top/bottom or left/right split
"Ctrl+W R
" Break out current window into a new tabview
"Ctrl+W T
" Close every window in the current tabview but the current one
"Ctrl+W o
set wmh=0

"---- Syntax Highlight
"set background=dark
"colorscheme carbonized-dark
"syntax off

if (exists('+colorcolumn'))
    set colorcolumn=80
    highlight ColorColumn ctermbg=233
endif

highlight Normal ctermfg=246
highlight Number ctermfg=251


"---- Plugin Conf

"-- NERDTree
" To have NERDTree always open on startup
"let g:nerdtree_tabs_open_on_console_startup = 1 " <--- FAIL
autocmd VimEnter * NERDTree
" Window width
let g:NERDTreeWinSize = 25

"-- Easy-align
vnoremap <silent> <Enter> :EasyAlign<cr>

"-- NeoMake
" Run Neomake when enter/saving buffer and leaving Insert mode
autocmd! BufWritePost,BufEnter,InsertLeave * Neomake
" Automatically open the error window
let g:neomake_open_list = 2
let g:neomake_list_height = 5
" Warning symbol
let g:neomake_warning_sign = {
  \ 'text': 'W',
  \ 'texthl': 'WarningMsg',
  \ }
" Error symbol
let g:neomake_error_sign = {
  \ 'text': 'E',
  \ 'texthl': 'ErrorMsg',
  \ }

"-- Vim-Inactive
let g:diminactive_use_colorcolumn = 0
let g:diminactive_use_syntax = 1

"-- org-mode composer
au! BufWritePost,BufEnter,InsertLeave *.org silent !pandoc -f org -t html -o /tmp/<afile>:r.html <afile>

